const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const {getUser} = require('./db');
const {isAuthenticated} = require('./middleware');

require('dotenv').config();

const PORT = 3003 || process.env.PORT;

app.use(bodyParser.json());

app.post('/login', (req, res) => {
	const {username, password} = req.body || {};

	const user = getUser(username, password);

	if (!user) {
		return res.status(403).send('Wrong username or password!');
	}

	const token = jwt.sign({user}, process.env.PRIVATE_KEY);

	res.set({
		'Login-Token': token,
	});

	res.send('Loggined');
});

app.get('/greeting', isAuthenticated, (req, res) => {
	res.send('Greeting!');
});

app.listen(PORT, () => console.log(`Started at 127.0.0.1:${PORT}`));
const jwt = require('jsonwebtoken');
const {getUser} = require('../db');

module.exports = {
	isAuthenticated: (req, res, next) => {
		const {token} = req.headers;

		if (!token) {
			return res.status(403).send('Unauthorized!');
		}

		const {user} = jwt.verify(token, process.env.PRIVATE_KEY);

		if (!user) {
			return res.status(403).send('Unauthorized!');
		}

		const {username, password} = user;

		const fakeDbResp = getUser(username, password);

		if (!fakeDbResp) {
			return res.status(403).send('Unauthorized!');
		}

		next();
	},
}
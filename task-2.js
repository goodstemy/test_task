// Название класса Ctrl не объясняет суть данного контроллера и за что он отвечает
class Ctrl extends Controller {
  // ctx = context = ok
  constructor(ctx) {
     super(ctx);
  }

  async run() {
    /**
      1. Слишком широкая зона ответственности для одного класса/функции
      2. Order.Model.find() && User.Model.findOne не должны вызываться напрямую из контроллера, 
         возможно стоит использовать некую прослойку для моделей
    **/
    const orders = await Order.Model.find().select('id user_id');
    const users = [];
    let count = 0;

    // Перебирать подобным образом заказы очень плохая идея. Мы остановим выполнение следующих задач до завершения цикла
    for (let order of orders) {
       const user = await User.Model.findOne({id: order.user_id});

       // Это не большая проблема, но для читаемся и чтобы угодить линтеру я бы использовал if (user) {}, т.е. вариант со скобками
       if (user)
           users.push(user) // Точка с запятой? Линтер не ругается?
    }

    // Опять же синхронный цикл с ожиданием действий сохранения данных юзера
    // Как минимум - это стоило бы обернуть в Promise.all в надежде, что у нас будет немного данных
    // Как максимум - это выносить подобную обработку вида `await user.save` в другой сервис изменяя данные пользователей в сервисе,
    // который за это отвечает
    for (let user of users) {
       // Это не большая проблема, но для читаемся и чтобы угодить линтеру я бы использовал if (user) {}, т.е. вариант со скобками
       if (user.problem === false)
           count++;
       user.problem = true;
       await user.save();
    }

    // Пробелы вокруг ключа в объекте - это в пресете линтера?
    return { count };
  }
}

/**

Рефакторинг ниже:

**/

class OrdersProvider extends DBProvider {
  // some logic

  getOrdersByUserId(id) {
    return Order.Model.find().select(id);
  }
}

class UsersProvider extends DBProvider {
  // some logic

  getUsersByOrdersIds(ids = []) {
    return User.model.find(ids)
  }

  setProblemTrueByUserId(id) {
    return User.update({problem: true}).save(); // is this ok ?
  }
}

class OrdersController extends Controller {
  constructor() {
    this.ordersProvider = new OrdersProvider();
  }

  async run(request) {
    const {data: {id}} = request;

    const orders = await this.ordersProvider.getOrdersByUserId(id);

    if (!orders) {
      return [];
    }

    return orders.map(({id}) => id);
  }

}

class UsersController extends Controller {
  constructor() {
     this.usersProvider = new UsersProvider();
  }

  async run(request) {
    const {data: {ids}} = request;

    return this.usersProvider.getUsersByOrdersIds(ids);
  }

  async updateUser(request) {
    const {data: {id}} = request;

    return this.usersProvider.setProblemTrueByUserId(id);
  }
}

class MainController extends Controller {
  getCountOrdersByUserId(request) {
    // imagine http request here
    const orderIds = await OrdersController.run(request);
    // and here
    const users = await UsersController.run({data: {ids: orderIds}});

    let count = 0;

    for (let i = 0; i < users.length; i++) {
      if (users[i].problem) {
        count++;

        /**
          Здесь мы не ожидаем запрос

          Я вижу здесь пару вариантов решения:

          1. Отправка батчами в контроллер для разруливания зависания event loop'a
          2. Отправка всех данных сразу и ожидание успешной записи. Так как запись в базу нам ждать не надо
          3. При это спокойно может возникнуть ситуация, когда count верный, а в базу не записались значения.
             Таким образом можно сделать retryCount и попробовать выполнить этот запрос еще пару тройку раз до первого успешного выполнения офк
        **/

        // image http request here
        UsersController
          .updateUser({data: {users[i].id}})
          .catch(console.error);
      }
    }

    return count;
  }
}
